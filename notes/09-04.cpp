// -----------
// Fri,  4 Sep
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Lecture
	1 pm

Help Sessions
	W 6-8pm

Office Hours
	Glenn
		Austin
		T 2-3 pm

	Sid ('20)
		Austin
		M 7-8 pm

	Kevin ('22)
		Plano
		Th 2-3 pm

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)

Notes
	these notes will be on GitLab
*/

/*
everything has a Canvas component
*/
