// -----------
// Wed, 26 Aug
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Lecture
	1 pm

Help Sessions
	W 6-8pm

Office Hours
	Glenn
		Austin
		T 2-3 pm

	Sid ('20)
		Austin
		M 7-8 pm

	Kevin ('22)
		Plano
		Th 2-3 pm

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)

Notes
	these notes will be on GitLab
*/

/*
GitLab
	these notes
	code examples
	exercise solutions
*/

/*
blogs
	weekly blog
	any blog platform
	I'll assign 15, drop 5, you MUST do the LAST one
	submit the URL of a blog entry to Piazza
	submit the URL of a blog entry to Canvas
	all the entries, but the last, typical software engineering questions
	the last entry, will be you assessment of how the whole class went for you
	no partial credit
	completion grade
	Sun night

papers
	weekly papers
	on Perusall, collaborative tool
	I'll assign 15, drop 5, you MUST do the FIRST one
	submit the URL of a blog entry to Canvas
	no partial credit
	completion grade
	Persuall has an AI, you'll need a 2
	Sun night

quizzes
	daily quizzes
	on Canvas
	multiple choice
	I'll assign 30+N, drop N
	two-stage, two attempts
	first attempt, individual, in the first 4 min of class
	second attempt, collaborative, in the second 4 min, breakout rooms, share your screen, help each other
	Canvas will give you the average of the two attempts

projects
	5 projects, all in C++, all on HackerRank
	1st project, individual
	remaining projects MAY be done in pairs
	if you pair up, has to be a different partner every time
	partial credit

tests
	2 tests, on HackerRank, all writing code in C++
	no final
	two stage
	first attempt, individual, on one day, 75 pts
	second attempt, collaborative, on the next day, breakout rooms, share your screen, help each other, 50 pts
	it's necessary but NOT sufficient to pass the HackerRank tests to get full credit
*/

/*
ice breaker

1. exchange contact info
2. ask two questions, try to be creative
3. Google "cs371p spring 2020 final entry"
*/

/*
I use an attendance in Canvas
Canvas insists on putting that into the grade book
please IGNORE
*/
