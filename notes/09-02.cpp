// -----------
// Wed,  2 Sep
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Lecture
	1 pm

Help Sessions
	W 6-8pm

Office Hours
	Glenn
		Austin
		T 2-3 pm

	Sid ('20)
		Austin
		M 7-8 pm

	Kevin ('22)
		Plano
		Th 2-3 pm

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)

Notes
	these notes will be on GitLab
*/

/*
everything has a Canvas component
*/

/*
we covered assertions
broken tests can hide broken code
*/

// input

1 10
100 200
201 210
900 1000

/*
run cycle length on all the numbers in a range, report the max cycle length
*/

// output

1 10 20
100 200 125
201 210 89
900 1000 174

/*
the ways in which you can convey a certain amount input
	1. stop giving more input, expecting the read to eventually fail
	2. up front, how much input
	3. sentinel at the end
*/

/*
read eval print loop (REPL)
*/

/*
unit tests are tests that test components of a solution
white box tests
we'll use Google Test
*/

/*
acceptance tests test the overall behavior the program, in this case, input / output pair
black box texts
two files: RunCollatz.in and RunCollatz.out
*/
