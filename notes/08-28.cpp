// -----------
// Fri, 28 Aug
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Lecture
	1 pm

Help Sessions
	W 6-8pm

Office Hours
	Glenn
		Austin
		T 2-3 pm

	Sid ('20)
		Austin
		M 7-8 pm

	Kevin ('22)
		Plano
		Th 2-3 pm

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)

Notes
	these notes will be on GitLab
*/

/*
Docker is a container
it virtualizes and OS and a tool chain

to get you to use some tools
	1. you install those tools on your machine
	2. you remote login to the CS machines, they have the tools
	3. you run a Docker image
*/

int main () {
    using namespace std;
    cout << "Nothing to be done." << endl;
    return 0;}

int main () {
    std::cout << "Nothing to be done." << std::endl;
    return 0;}

/*
#include is NOT analogous to Java's import
*/

/*
using IS analogous to Java's import

/*
analagous to

java.math.sqrt
vs.
sqrt
*/

int i = 2;
int j = 3;
cout << i << " " << j; // 2 3

/*
what did
cout << i
return?

cout
*/

/*
Collatz Conjecture
about 100 years old

take a pos int
if even divide by 2
otherwise multiply by 3 and add 1
*/

5 16 8 4 2 1

/*
the cycle length of  5 is 6
the cycle length of 10 is 7
*/
