// -----------
// Mon, 31 Aug
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

Lecture
	1 pm

Help Sessions
	W 6-8pm

Office Hours
	Glenn
		Austin
		T 2-3 pm

	Sid ('20)
		Austin
		M 7-8 pm

	Kevin ('22)
		Plano
		Th 2-3 pm

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Zoom
	must use UT EID credentials (@eid.utexas.edu)

Notes
	these notes will be on GitLab
*/

/*
please check your grades on Canvas regularly
*/

/*
Collatz Conjecture
about 100 years old

take a pos int
if even divide by 2
otherwise multiply by 3 and add 1
*/

5 16 8 4 2 1

/*
the cycle length of  5 is 6
the cycle length of 10 is 7
the cycle length of  3 is 8
*/

int cycle_length (int n) {
    assert(n > 0);            // precondition
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);            // postconditon
    return c;}

int i = 2;
int j = ++i;
cout << i;   // 3
cout << j;   // 3

++i;
i + j;         // no
int k = i + j;
i += j;
++++i;         // ok!
++(i + j);     // no!

int i = 2;
int j = i++;
cout << i;   // 3
cout << j;   // 2

i++++;       // no!

void test () {
    assert(cycle_length( 1) == 1);  // using it as a unit test
    assert(cycle_length( 5) == 6);
    assert(cycle_length(10) == 7);}

int main () {
    cout << "Assertions.c++" << endl;
    test();
    cout << "Done." << endl;
    return 0;}

/*
% g++-10 -pedantic -std=c++17 -O3 -Wall Assertions.cpp -o Assertions
% ./Assertions
Assertions.c++
Assertion failed: (c > 0), function cycle_length, file Assertions.cpp, line 21.



% g++-10 -pedantic -std=c++17 -O3 -DNDEBUG -Wall Assertions.cpp -o Assertions
% ./Assertions
Assertions.c++
Done.
*/

/*
assertions
	are computational comment
	forced to keep up to date
	are NOT good for are unit tests
		what is good is unit test framework, Google Test
	are NOT good for user errors
		what is good are exceptions
*/

/*
HackerRank exercise
15 breakout rooms
	Glenn: 1-5
	Kevin: 6-10
	Sid:   11-15
share your screen and discuss
NO need to submit
keep your screen up
you do NOT need a HackerRank account
use any e-mail, a new e-mail for every attempt
use 5182
your name
your EID
*/

/*
broken tests can hide broken code

1. run the code as is, confirm success
2. identify and fix tests
3. run it again, confirm failure
4. identify and fix code
5. run it again, confirm success
*/

bool is_prime (int n) {
    assert(n > 0);
    if (n == 2)
    	return true;
    if ((n == 1) || ((n % 2) == 0))
        return false;
    for (int i = 3; i < std::sqrt(n) + 1; ++++i)
        if ((n % i) == 0)
            return false;
    return true;}

void test0 () {
    assert(!is_prime( 1));}

void test1 () {
    assert(!is_prime( 2));} // broken

void test2 () {
    assert( is_prime( 3));}

void test3 () {
    assert(!is_prime( 4));}

void test4 () {
    assert( is_prime( 5));}

void test5 () {
    assert( is_prime( 7));}

void test6 () {
    assert( is_prime( 9));} // broken

void test7 () {
    assert(!is_prime(27));}

void test8 () {
    assert( is_prime(29));}
